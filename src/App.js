import React, {useState, useReducer} from 'react';
import logo from './logo.svg';
import './App.css';

const reducer = (state, action) => {
  switch(action.type) {
    case "INCREMENT":
      return {count: state.count + 1, showText: state.showText};
    case "toggleShowText":
      return {count: state.count, showText: !state.showText};
    default:
      return state;
  }
}

const App = () => {
  const [counter, setCounter] = useState(0);
  const [inputValue, setInputValue] = useState('Hashmi');

  const [state, dispatch] = useReducer(reducer, {count: 0, showText: false})

  
  const onChange = (e) => {
    let newValue = e.target.value;
    if (newValue == ''){
      newValue = 'Hashmi';
    }
    setInputValue(newValue);
  }
  
  const increment = () => {
    setCounter(counter + 1);
  }

  const decrement = () => {
    setCounter(counter - 1);
  }

  const clickReducer = () => {
    dispatch({type: "INCREMENT"});
    dispatch({type: "toggleShowText"});
  }

  return (
    <div className="App">
      <div>{counter}</div>
      <button onClick={increment}>increment</button>
      <button onClick={decrement}>Decrement</button>
      <br/><br/><br/>

      <input placeholder="enter something.." onChange={onChange}/>
      <p>{inputValue}</p>


      <br/><br/><br/>

      <h1>{state.count}</h1>
      <button onClick={clickReducer}>toggle reducer function</button>
      {state.showText && <p>JGM</p>}




    </div>
  );
}

export default App;
